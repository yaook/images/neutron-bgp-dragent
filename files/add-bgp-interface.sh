#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

# This script is used at bgp-dragent-statefulset.yaml of bgp-operator at the
# init container. It creates a port at the given bridge and connects it to the
# pods network namespace, so bgp agent can reach the bgp peer via the
# "external" network.

set -e
set -u
set -o pipefail

if [[ "${DEBUG:-0}" == "1" ]]; then
    set -x
fi

# shellcheck disable=SC1091
. /etc/neutron/bgp_mapping

OVS_SOCKET="${OVS_SOCKET:-unix:/run/openvswitch/db.sock}"

function _check_bgp_mapping_variables() {
    for not_empty_var_name in "BRIDGE" "BGP_INTERFACE" "BGP_IP"; do
        if [[ "${!not_empty_var_name}" == "" ]]; then
            echo "# bgp_mapping variable $not_empty_var_name is empty. Exiting with error."
            exit 1
        fi
    done
}

function _delete_bgp_interface() {
    # Remove interface at ovs, if cleanup of bgp-agent container failed
    echo "# Try to delete interface ${BGP_INTERFACE} from bridge ${BRIDGE}."
    ovs-vsctl --db="${OVS_SOCKET}" \
              --if-exists \
              del-port "${BRIDGE}" "${BGP_INTERFACE}"
}

function _create_bgp_interface_in_container_network_namespace() {
    echo "# creating bgp interface ${BGP_INTERFACE} at root namespace"
    # We fail here, if the bridge is not created yet.
    # The bridge is created by l2-agent pod, which should be there before
    # creating the bgp-agent pod
    # k8s will restart the pod and we retry
    ovs-vsctl --db="${OVS_SOCKET}" \
                add-port "${BRIDGE}" "${BGP_INTERFACE}" \
                -- set Interface "${BGP_INTERFACE}" type=internal

    echo "# Assigning bgp interface ${BGP_INTERFACE} to container network namespace 1 (/proc/1/ns/net)."
    # this expect the hosts /proc mounted to /host/proc, so we can enter the hosts network namespace
    nsenter --net=/host/proc/1/ns/net ip link set "${BGP_INTERFACE}" netns /proc/$$/ns/net

    # Enable ipv6 support.
    # Container have ipv6 disabled if not explicit enabled in the container runtime.
    echo 0 > /proc/sys/net/ipv6/conf/"${BGP_INTERFACE}"/disable_ipv6

    echo "# Assigning ip ${BGP_IP} to interface ${BGP_INTERFACE}"
    ip address add "${BGP_IP}" dev "${BGP_INTERFACE}"
    echo "# Set up interface ${BGP_INTERFACE}"
    ip link set up "${BGP_INTERFACE}"
}

function main() {
    # main entrypoint of script
    echo "# Starting $0"

    _check_bgp_mapping_variables
    _delete_bgp_interface
    _create_bgp_interface_in_container_network_namespace

    echo "# Finished $0"
}

main

