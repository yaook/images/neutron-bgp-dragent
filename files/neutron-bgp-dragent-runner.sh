#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

set -ex

# shellcheck disable=SC1091
. /etc/neutron/bgp_mapping

OVS_SOCKET="${OVS_SOCKET:-unix:/run/openvswitch/db.sock}";

echo "[bgp]" > /tmp/ipoverwrite.conf
echo "bgp_router_id = ${NODE_IP}" >> /tmp/ipoverwrite.conf

cat /tmp/ipoverwrite.conf

_term() {
    echo "# Caught SIGTERM signal."
    kill -TERM "$child" 2>/dev/null
    echo "# Delete ovs interface for bgp ${BGP_INTERFACE} from bridge ${BRIDGE}."
    ovs-vsctl --db="${OVS_SOCKET}" --no-wait del-port "${BRIDGE}" "${BGP_INTERFACE}";
}

# We want to cleanup the interface created with add-bgp-interface.sh to have
# no leftover devices there.
# Badly we can only react at SIGTERM, at SIGKILL we have no chance to react.
# In this case, the interface will be still at ovs bridge (and at some network
# namespace, that don't exist anymore), so no harm from that.
# The leftover interface will NOT be present at the root namespace.
# At reboot of the node, the interface gets removed from ovs bridge/not recreated.
# If the pod gets restarted, the interface will also be cleaned up and
# recreated by the init container.
trap _term SIGTERM

neutron-bgp-dragent --config-file /etc/neutron/neutron.conf --config-file /tmp/ipoverwrite.conf &

child=$!
wait "$child"
