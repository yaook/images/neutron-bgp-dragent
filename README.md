<!--
Copyright (c) 2021 The Yaook Authors.

This file is part of Yaook.
See https://yaook.cloud for further info.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->
# neutron-bgp-dragent

The docker image for the openstack neutron bgp-dragent

## License

[Apache 2](LICENSE.txt)

### add-bgp-interface

This script is used at bgp-dragent-statefulset.yaml of bgp-operator at the
init container. It creates a port at the given bridge and connects it to the
pods network namespace, so bgp agent can reach the bgp peer via the "external"
network.
