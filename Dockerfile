##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
ARG release=train

FROM registry.yaook.cloud/yaook/neutron-agent-${release}:1.0.51

# Because ARGs defined before a FROM are not available after the FROM,
# ARGs that should be available after the FROM must be re-defined.
ARG release
ARG branch=stable/$release

RUN (git clone https://github.com/openstack/requirements.git --depth 1 --branch ${branch} || \
    git clone https://github.com/openstack/requirements.git --depth 1 --branch ${release}-eol) && \
    git clone https://github.com/openstack/neutron-dynamic-routing.git --depth 1 --branch ${branch} || \
    git clone https://github.com/openstack/neutron-dynamic-routing.git --depth 1 --branch ${release}-eol && \
    pip3 install -c requirements/upper-constraints.txt neutron-dynamic-routing/ && \
    rm -rf requirements neutron-dynamic-routing;

COPY files/*.sh /
